import time
import urllib2
import re
from threading import Timer, Thread, Event

class MyInterface(object):
    def __init__(self):
        self.is_fresh = False
        self._my_value = 0

    @property
    def my_value(self):
        if not self.is_fresh:
            response = urllib2.urlopen('http://unixtime.org/')
            html = response.read()
            result = re.search(r'Current unix time: ([0-9]+)', html)
            self._my_value = result.group(1)
            self.is_fresh = True
        return self._my_value

    @my_value.setter
    def my_value(self, value):
        self._my_value = value

    @my_value.deleter
    def my_value(self):
        self.is_fresh = False


class MyThread(Thread):
    def __init__(self, event, value_object):
        super().__init__(self)
        self.stopped = event
        self.value_object = value_object
        self.delay = 2

    def run(self):
        print ("start!")
        while not self.stopped.wait(self.delay):
            del self.value_object.my_value
            #print (self.delay)
            if self.delay < 7:
                self.delay += 1
            else:
                self.delay = 2
        print ("end!")



interface = MyInterface()
stopFlag = Event()
thread = MyThread(stopFlag, interface)
thread.start()

while True:
    time.sleep(1)
    print(pr.my_value)